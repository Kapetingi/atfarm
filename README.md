# Solution info

This solution implement O(1) for /field-statistics endpoint. It accumulate vegetation data during
all addFieldCondition POST method call and recalculate min, max and avr value.

for run it just run 

mvn spring-boot:run

Solution also store data for each measure in H2 data base. During bootstrap data loads from DB. 

The interface of solution should be available via swagger-ui on endpoint http://localhost:8080/swagger-ui.html

The open api documentation is present on /src/main/resources/api.yaml




