package at.farm.monitor;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class TestFieldController {

    @Autowired
    private MockMvc mockMvc;

    @Test
    @DirtiesContext
    public void statisticField() throws Exception {
        this.mockMvc.perform(get("/field-statistics")).andExpect(status().isOk())
                .andExpect(content().string(containsString("{\"vegetation\":{\"min\":0.10,\"max\":0.90,\"avg\":0.60}}")));
    }


    @Test
    @DirtiesContext
    public void addFieldCondition() throws Exception {

        this.mockMvc.perform(post("/field-conditions")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\n" +
                        "  \"occurrenceAt\": \"2019-07-12T21:15:46.787Z\",\n" +
                        "  \"vegetation\": 0\n" +
                        "}"))
                .andExpect(status().isCreated());
    }

    @Test
    @DirtiesContext
    public void addFieldBadCondition() throws Exception {

        this.mockMvc.perform(post("/field-conditions")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\n" +
                        "  \"occurrenceAt\": \"2019-07-12T21:15:46.787Z\",\n" +
                        "  \"vegetation\": -1\n" +
                        "}"))
                .andExpect(status().isBadRequest())
                .andExpect(status().reason(containsString("Vegetation should be positive value")));
    }

}
