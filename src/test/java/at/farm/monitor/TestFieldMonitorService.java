package at.farm.monitor;

import at.farm.monitor.exception.NegativeVegetationValueException;
import at.farm.monitor.model.FieldStatisticDTO;
import at.farm.monitor.model.VegetationMeasureDTO;
import at.farm.monitor.service.FieldStatisticService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = MonitorApplication.class)
public class TestFieldMonitorService {

    @Autowired
    FieldStatisticService fieldStatisticService;


    @Test
    @DirtiesContext
    public void testStatisticServiceInitialization(){
        FieldStatisticDTO statistic = fieldStatisticService.getStatistic();
        Assert.assertNotNull(statistic);
        Assert.assertEquals(new BigDecimal("0.10"), statistic.getMin());
        Assert.assertEquals(new BigDecimal("0.90"), statistic.getMax());
        Assert.assertEquals(new BigDecimal("0.60"), statistic.getAvg());
    }

    @Test
    @DirtiesContext
    public void testStatisticServiceAddMeasure() throws NegativeVegetationValueException {
        fieldStatisticService.addMeasure(new VegetationMeasureDTO(new BigDecimal("1.20"), ZonedDateTime.now()));
        fieldStatisticService.addMeasure(new VegetationMeasureDTO(new BigDecimal("0.05"), ZonedDateTime.now()));
        FieldStatisticDTO statistic = fieldStatisticService.getStatistic();
        Assert.assertNotNull(statistic);
        Assert.assertEquals(new BigDecimal("0.05"), statistic.getMin());
        Assert.assertEquals(new BigDecimal("1.20"), statistic.getMax());
        Assert.assertEquals(new BigDecimal("0.61"), statistic.getAvg());
    }

    @Test
    @DirtiesContext
    public void testStatisticAddOutdatedMeasure() throws NegativeVegetationValueException {
        fieldStatisticService.addMeasure(new VegetationMeasureDTO(new BigDecimal("0.05"), ZonedDateTime.now().minus(31, ChronoUnit.DAYS)));
        FieldStatisticDTO statistic = fieldStatisticService.getStatistic();
        Assert.assertNotNull(statistic);
        Assert.assertEquals(new BigDecimal("0.10"), statistic.getMin());
        Assert.assertEquals(new BigDecimal("0.90"), statistic.getMax());
        Assert.assertEquals(new BigDecimal("0.60"), statistic.getAvg());
    }


    @Test(expected = NullPointerException.class)
    @DirtiesContext
    public void testNullVegetation() throws NegativeVegetationValueException {
        fieldStatisticService.addMeasure(new VegetationMeasureDTO(null, ZonedDateTime.now().minus(31, ChronoUnit.DAYS)));
    }

    @Test(expected = NullPointerException.class)
    @DirtiesContext
    public void testNullOccurrence() throws NegativeVegetationValueException {
        fieldStatisticService.addMeasure(new VegetationMeasureDTO(new BigDecimal("0.05"), null));
    }

}
