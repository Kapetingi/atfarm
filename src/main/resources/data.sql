-- init data
insert into vegetation_measure (id, occurrence_at, vegetation) values (1, now(), 0.90);
insert into vegetation_measure (id, occurrence_at, vegetation) values (2, now(), 0.80);
insert into vegetation_measure (id, occurrence_at, vegetation) values (3, now(), 0.10);