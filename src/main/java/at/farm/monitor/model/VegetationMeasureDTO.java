package at.farm.monitor.model;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.time.ZonedDateTime;

@Getter
@Setter
public class VegetationMeasureDTO {

    private BigDecimal vegetation;
    private ZonedDateTime occurrenceAt;

    public VegetationMeasureDTO() {

    }

    public VegetationMeasureDTO(BigDecimal vegetation, ZonedDateTime occurrenceAt) {
        this.vegetation = vegetation;
        this.occurrenceAt = occurrenceAt;
    }

}
