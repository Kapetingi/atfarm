package at.farm.monitor.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeName;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
@JsonTypeName("vegetation")
@JsonTypeInfo(include = JsonTypeInfo.As.WRAPPER_OBJECT ,use = JsonTypeInfo.Id.NAME)
public class FieldStatisticDTO {

    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private BigDecimal min;
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private BigDecimal max;
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private BigDecimal avg;


    public FieldStatisticDTO(BigDecimal min, BigDecimal max, BigDecimal avg) {
        this.min = min;
        this.max = max;
        this.avg = avg;
    }
}
