package at.farm.monitor.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "Vegetation should be positive value")
public class NegativeVegetationValueException extends Exception {


    static final long serialVersionUID = -3387516923224229948L;

    public NegativeVegetationValueException(String message)
    {
        super(message);
    }
}
