package at.farm.monitor.api;

import at.farm.monitor.exception.NegativeVegetationValueException;
import at.farm.monitor.model.FieldStatisticDTO;
import at.farm.monitor.model.VegetationMeasureDTO;
import at.farm.monitor.service.FieldStatisticService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class FieldController {

    private final FieldStatisticService fieldStatisticService;

    @Autowired
    public FieldController(FieldStatisticService fieldStatisticService) {
        this.fieldStatisticService = fieldStatisticService;
    }


    @GetMapping("/field-statistics")
    public ResponseEntity<FieldStatisticDTO> getFieldStatistic() {
        return ResponseEntity.ok(fieldStatisticService.getStatistic());
    }


    @PostMapping("/field-conditions")
    @ResponseStatus(HttpStatus.CREATED)
    public void addFieldCondition(@RequestBody VegetationMeasureDTO vegetationMeasureDTO) throws NegativeVegetationValueException {
        fieldStatisticService.addMeasure(vegetationMeasureDTO);
    }


}
