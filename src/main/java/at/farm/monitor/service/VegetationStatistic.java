package at.farm.monitor.service;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
public class VegetationStatistic {

    private BigDecimal max;
    private BigDecimal min;
    private Long statisticCount;
    private BigDecimal sum;

    public VegetationStatistic(BigDecimal max, BigDecimal min, Long statisticCount, BigDecimal sum) {
        this.max = max;
        this.min = min;
        this.statisticCount = statisticCount;
        this.sum = sum;
    }
}
