package at.farm.monitor.service;

import at.farm.monitor.dao.VegetationMeasure;
import at.farm.monitor.dao.VegetationMeasureRepository;
import at.farm.monitor.exception.NegativeVegetationValueException;
import at.farm.monitor.model.FieldStatisticDTO;
import at.farm.monitor.model.VegetationMeasureDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.*;

@Service
public class DefaultFieldStatisticService implements FieldStatisticService {

    private final VegetationMeasureRepository vegetationRepository;

    private final Map<LocalDate, VegetationStatistic> vegetationStatisticMap;

    @Autowired
    public DefaultFieldStatisticService(VegetationMeasureRepository vegetationRepository) {
        this.vegetationRepository = vegetationRepository;
        vegetationStatisticMap = new HashMap<>();
        initialization();
    }

    /**
     * procedure for initalization current statistic from DB data
     */
    private void initialization() {
        List<VegetationMeasure> vegetationList = new ArrayList<>();
        vegetationRepository.findAllForMonthOrdered(ZonedDateTime.now().minus(30, ChronoUnit.DAYS)).forEach(vegetationList::add);
        if (vegetationList.size() == 0)
            return;
        BigDecimal sum = vegetationList.stream()
                .map(VegetationMeasure::getVegetation)
                .reduce(BigDecimal.ZERO, BigDecimal::add);
        vegetationStatisticMap.put(LocalDate.now(),
                new VegetationStatistic(vegetationList.get(vegetationList.size() - 1).getVegetation(),
                        vegetationList.get(0).getVegetation(),
                        (long) vegetationList.size(), sum));
    }

    public FieldStatisticDTO getStatistic() {
        VegetationStatistic vegetationStatistic = vegetationStatisticMap.get(LocalDate.now());
        if (vegetationStatistic != null) {
            return new FieldStatisticDTO(vegetationStatistic.getMin(),
                    vegetationStatistic.getMax(),
                    vegetationStatistic.getSum().divide(BigDecimal.valueOf(vegetationStatistic.getStatisticCount()), RoundingMode.FLOOR));
        }
        return new FieldStatisticDTO(BigDecimal.ZERO, BigDecimal.ZERO, BigDecimal.ZERO);
    }

    public synchronized void addMeasure(VegetationMeasureDTO measure) throws NegativeVegetationValueException {
        Objects.requireNonNull(measure.getVegetation(), "Vegetation should be not null");
        Objects.requireNonNull(measure.getOccurrenceAt(), "Date of occurrence should be not null");
        if (measure.getVegetation().compareTo(BigDecimal.ZERO) < 0) {
            throw new NegativeVegetationValueException("Vegetation should be positive value");
        }

        VegetationStatistic currentStatistic = vegetationStatisticMap.remove(measure.getOccurrenceAt().toLocalDate());
        if (currentStatistic == null && isDateOnThisMonth(measure.getOccurrenceAt())) {
            currentStatistic = new VegetationStatistic(measure.getVegetation(), measure.getVegetation(), 1L, measure.getVegetation());
            vegetationStatisticMap.put(LocalDate.now(), currentStatistic);
        } else if (currentStatistic != null) {
            vegetationStatisticMap.put(LocalDate.now(), updateStatistic(currentStatistic, measure));
        }
        saveMeasure(measure);
    }


    private boolean isDateOnThisMonth(ZonedDateTime occurrence) {
        return occurrence.toLocalDate().compareTo(LocalDate.now().minus(30, ChronoUnit.DAYS)) >= 0
                && occurrence.toLocalDate().compareTo(LocalDate.now()) <= 0;
    }


    private void saveMeasure(VegetationMeasureDTO measure) {
        VegetationMeasure vegetation = new VegetationMeasure();
        vegetation.setOccurrenceAt(measure.getOccurrenceAt());
        vegetation.setVegetation(measure.getVegetation());
        vegetationRepository.save(vegetation);
    }

    private VegetationStatistic updateStatistic(VegetationStatistic currentStatistic, VegetationMeasureDTO measure) {
        if (currentStatistic.getMax().compareTo(measure.getVegetation()) < 0) {
            currentStatistic.setMax(measure.getVegetation());
        }
        if (currentStatistic.getMin().compareTo(measure.getVegetation()) > 0) {
            currentStatistic.setMin(measure.getVegetation());
        }
        currentStatistic.setSum(currentStatistic.getSum().add(measure.getVegetation()));
        currentStatistic.setStatisticCount(currentStatistic.getStatisticCount() + 1);
        return currentStatistic;
    }


}
