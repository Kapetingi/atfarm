package at.farm.monitor.service;

import at.farm.monitor.exception.NegativeVegetationValueException;
import at.farm.monitor.model.FieldStatisticDTO;
import at.farm.monitor.model.VegetationMeasureDTO;

import javax.validation.constraints.NotNull;

public interface FieldStatisticService {

    /**
     * Add measure to current statistic and store it in db
     * @param vegetationMeasure measure of vegetation
     */
    void addMeasure(@NotNull VegetationMeasureDTO vegetationMeasure) throws NegativeVegetationValueException;

    /**
     * Provide information about vegetation statistic on current date
     * @return statistic info(min,max,avr)
     */
    @NotNull
    FieldStatisticDTO getStatistic();
}
