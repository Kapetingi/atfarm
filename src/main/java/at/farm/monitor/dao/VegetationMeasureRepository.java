package at.farm.monitor.dao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.Optional;

public interface VegetationMeasureRepository extends CrudRepository<VegetationMeasure, Long> {

    Optional<VegetationMeasure> findByOccurrenceAt(ZonedDateTime occurrenceAt);

    @Query("SELECT vg from VegetationMeasure vg where vg.occurrenceAt>=:startDate order by vg.vegetation asc")
    List<VegetationMeasure> findAllForMonthOrdered(@Param("startDate") ZonedDateTime startDate);
}
