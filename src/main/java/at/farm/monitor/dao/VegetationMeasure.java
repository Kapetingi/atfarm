package at.farm.monitor.dao;

import lombok.Getter;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.ZonedDateTime;

@Entity
@Table(name="vegetation_measure")
@Getter
@Setter
public class VegetationMeasure {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private ZonedDateTime occurrenceAt = ZonedDateTime.now();

    @Column(nullable = false)
    private BigDecimal vegetation;


}
